#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <memory>
#include <iomanip>

constexpr const char* WORD_LIST_FILE_PATH = "res/words.txt";

class TypingGame
{
private:
   using clock = std::chrono::high_resolution_clock;
   using duration = std::chrono::duration<long, std::ratio<1, 1000>>;
   using tpoint = std::chrono::time_point<clock, duration>;

public:
   TypingGame() : rng(std::chrono::high_resolution_clock::now().time_since_epoch().count())
   {
      word_list = new std::vector<std::string>();
      (*word_list).reserve(466544);
      load_words();
      uni = std::uniform_int_distribution<int>(0, (*word_list).size());
   }

   void start()
   {
      auto now = []() { return std::chrono::time_point_cast<duration, std::chrono::system_clock, std::chrono::nanoseconds>(clock::now()); };

      duration total_duration;

      for (short i = 1; i <= 60; i++) {
         pick_random_word();
         std::cout << (*picked_word) << std::endl;
         std::string input("");

         tpoint start = now();
         while (input != (*picked_word)) {
            std::cin >> input;
         }
         tpoint end = now();
         auto time_taken = end - start;
         total_duration += time_taken;
         print_duration(total_duration);
         std::cout << " | " << i << "/"
                   << "60" << std::endl;
      }
      print_duration(total_duration);
      double wpm = (double)60 / ((double)total_duration.count() / 60000);
      std::cout << /*std::setprecision(2) <<*/ "\nThat means: " << wpm << " words per minute" << std::endl;
   }

private:
   void print_duration(duration dur)
   {
      long minutes = dur.count() / 60000;
      long seconds = (dur.count() / 1000) % 60;
      long milsecs = dur.count() % 1000;
      std::cout << "Total duration: " << minutes << "m " << seconds << "s " << milsecs << "ms" << std::flush;
   }
   void pick_random_word()
   {
      int index = uni(rng);
      auto it = (*word_list).begin();
      std::advance(it, index);
      picked_word = it;
   }
   void load_words()
   {
      std::ifstream word_stream(WORD_LIST_FILE_PATH);
      std::string temp_word;
      while (word_stream >> temp_word) {
         (*word_list).push_back(temp_word);
      }
      word_stream.close();
   }

   std::mt19937 rng;
   std::uniform_int_distribution<int> uni;
   std::vector<std::string>::iterator picked_word;
   std::vector<std::string>* word_list;
};

int main(int, char**)
{
   TypingGame game;
   game.start();
   return 0;
}
