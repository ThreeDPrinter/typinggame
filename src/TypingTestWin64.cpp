#include "stdafx.h"
#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <fstream>
//#include <iterator>
//#include <memory>
//#include <iomanip>
#include <Windows.h>
#include <string>
//hi
constexpr const char* WORD_LIST_FILE_PATH = "res/words.txt";
static std::string picked_word = "";
static unsigned __int64 freq;
static double timerFrequency = 0;
static unsigned __int64 startTime;
static std::vector<std::string> word_list;
static unsigned short wordlimit = 10;


class TypingGame
{
private:
	//using clock = std::chrono::high_resolution_clock;
	//using duration = std::chrono::duration<long, std::ratio<1, 1000>>;
	//using tpoint = std::chrono::time_point<clock, duration>;

public:
	TypingGame()
	
	{
		//word_list = new std::vector<std::string>();
		std::vector<std::string> word_list;
		word_list.reserve(466544);
		load_words();
		//uni = std::uniform_int_distribution<int>(0, word_list.size());
	}

	unsigned __int64 now() {
		QueryPerformanceCounter((LARGE_INTEGER *)&startTime);
		return startTime;
	}


	void start()
	{

		 
	//	auto now = []() { return std::chrono::time_point_cast<duration, std::chrono::system_clock, std::chrono::nanoseconds>(clock::now()); }; Not VS compatible 

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		timerFrequency = (1.0 / freq);

		double total_duration = 0;
	
		for (unsigned short i = 1; i <= wordlimit; i++) {


			pick_random_word();
		
			std::cout << picked_word << std::endl;
			std::string input = "";
		

			unsigned __int64 start= now();
			while (input != picked_word) {
				std::cin >> input;
			}
			unsigned __int64 end = now();
			double time_taken = ((end - start) * timerFrequency);
			
			total_duration += time_taken;
			std::cout << total_duration << std::endl;
			print_duration(total_duration*1000);
			std::cout << " | " << i << "/"
				<< wordlimit << std::endl;
		}

		unsigned long tprint = total_duration*1000;
		print_duration(tprint);
		double wpm = (double)wordlimit / ((double)(total_duration*1000)/ 60000);
		std::cout << /*std::setprecision(2) <<*/ "\nThat means: " << wpm << " words per minute" << std::endl;
	}

private:
	void print_duration(long dur)
	{

		
		unsigned long minutes = dur / 60000;
		unsigned long seconds = (dur/1000) % 60;
		unsigned long milsecs = dur % 1000;
		std::cout << "Total duration: " << minutes << "m " << seconds << "s " << milsecs << "ms" << std::flush;
	}
	void pick_random_word()
	{
	

		unsigned long long r = 0;				//rand generates 0 to 32,767, this is 15 bits
		for (int i = 0; i < 5; i++) {			//this is done 5 times with bitwise or shifting
			std::srand(time(0));
			r = (r << 15) | (rand() & 0x7FFF);
		}									
		unsigned int ran = 0;
		ran = (r % word_list.size());
		picked_word = word_list[ran];
	
		/*	int index = uni(rng);	//Reimplemented
		auto it = (*word_list).begin();
		std::advance(it, index);
		picked_word = it;
		*/
		

	}
	void load_words()
	{
		const char * filename = "res\\words.txt";
		std::ifstream input(filename);
		if (!input.is_open()) {
			std::cerr << "Can't open " << filename << "!\n";
			Sleep(2000);
			exit(EXIT_FAILURE);
		}
		std::string line;
		for (std::string line; std::getline(input, line); )
		{
			word_list.push_back(line);
		}
		input.close();
	}

	//std::mt19937 rng; 
	//std::uniform_int_distribution<int> uni;
	//std::vector<std::string>::iterator picked_word;
	
};

int main(int, char**)
{
	TypingGame game;
	game.start();
	return 0;
}
